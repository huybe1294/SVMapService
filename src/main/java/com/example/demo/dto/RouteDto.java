package com.example.demo.dto;

import javax.persistence.*;

@Entity
@Table(name = "routes")
public class RouteDto {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "route_id")
    private Integer routeId;

    @Column(name = "route_name")
    private String routeName;

    public RouteDto() {
    }

    public RouteDto(String routeName) {
        this.routeName = routeName;
    }

    public Integer getRouteId() {
        return routeId;
    }

    public String getRouteName() {
        return routeName;
    }
}
