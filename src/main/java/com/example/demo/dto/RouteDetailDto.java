package com.example.demo.dto;

import javax.persistence.*;

@Entity
@Table(name = "route_details")
public class RouteDetailDto {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private int id;

    @Column(name = "route_id")
    private int routeId;

    @Column(name = "stop_id")
    private int stopId;

    public RouteDetailDto() {
    }

    public RouteDetailDto(int routeId, int stopId) {
        this.routeId = routeId;
        this.stopId = stopId;
    }

    public int getId() {
        return id;
    }

    public int getRouteId() {
        return routeId;
    }

    public int getStopId() {
        return stopId;
    }
}
