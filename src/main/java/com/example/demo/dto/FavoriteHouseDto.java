package com.example.demo.dto;

import javax.persistence.*;

@Entity
@Table(name = "favorite_house")
public class FavoriteHouseDto {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Integer id;

    @Column(name = "user_id")
    private Integer user_id;

    @Column(name = "house_id")
    private Integer house_id;

    public FavoriteHouseDto() {
    }

    public FavoriteHouseDto(Integer user_id, Integer job_id) {
        this.user_id = user_id;
        this.house_id = job_id;
    }

    public Integer getId() {
        return id;
    }

    public Integer getUser_id() {
        return user_id;
    }

    public Integer getHouse_id() {
        return house_id;
    }
}
