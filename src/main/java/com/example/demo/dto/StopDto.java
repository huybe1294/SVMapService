package com.example.demo.dto;

import javax.persistence.*;

@Entity
@Table(name = "stops")
public class StopDto {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "stop_id")
    private Integer stopId;

    @Column(name = "stop_name")
    private String stopName;

    @Column(name = "latitude")
    private String latitude;

    @Column(name = "longitude")
    private String longitude;


    public StopDto() {
    }

    public StopDto(String stopName, String latitude, String longitude) {
        this.stopName = stopName;
        this.latitude = latitude;
        this.longitude = longitude;
    }

    public Integer getStopId() {
        return stopId;
    }

    public String getStopName() {
        return stopName;
    }

    public String getLatitude() {
        return latitude;
    }

    public String getLongitude() {
        return longitude;
    }
}
