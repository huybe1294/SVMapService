package com.example.demo.dto;

import javax.persistence.*;

@Entity
@Table(name = "favorite_job")
public class FavoriteJobDto {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Integer id;

    @Column(name = "user_id")
    private Integer user_id;

    @Column(name = "job_id")
    private Integer job_id;

    public FavoriteJobDto() {
    }

    public FavoriteJobDto(Integer user_id, Integer job_id) {
        this.user_id = user_id;
        this.job_id = job_id;
    }

    public Integer getId() {
        return id;
    }

    public Integer getUser_id() {
        return user_id;
    }

    public Integer getJob_id() {
        return job_id;
    }
}
