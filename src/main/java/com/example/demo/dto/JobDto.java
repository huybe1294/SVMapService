package com.example.demo.dto;

import javax.persistence.*;

@Entity
@Table(name = "jobs")
public class JobDto implements Comparable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Integer id;

    @Column(name = "title")
    private String title;

    @Column(name = "company_name")
    private String companyName;

    @Column(name = "phone")
    private String phone;

    @Column(name = "salary")
    private String salary;

    @Column(name = "benefit")
    private String benefit;

    @Column(name = "describe")
    private String describe;

    @Column(name = "latitude")
    private Double latitude;

    @Column(name = "longitude")
    private Double longitude;

    @Column(name = "requirements")
    private String requirements;

    @Column(name = "start_date")
    private Long startDate;

    @Column(name = "end_date")
    private Long endDate;

    @Column(name = "favorite")
    private Integer favorite;

    @Column(name = "address")
    private String address;

    public JobDto() {
    }

    public JobDto(String title, String companyName, String phone, String salary, String benefit, String describe, Double latitude, Double longitude, String requirements, Long startDate, Long endDate, Integer favorite, String address) {
        this.title = title;
        this.companyName = companyName;
        this.phone = phone;
        this.salary = salary;
        this.benefit = benefit;
        this.describe = describe;
        this.latitude = latitude;
        this.longitude = longitude;
        this.requirements = requirements;
        this.startDate = startDate;
        this.endDate = endDate;
        this.favorite = favorite;
        this.address = address;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public Integer getFavorite() {
        return favorite;
    }

    public void setFavorite(Integer favorite) {
        this.favorite = favorite;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getCompanyName() {
        return companyName;
    }

    public void setCompanyName(String companyName) {
        this.companyName = companyName;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getSalary() {
        return salary;
    }

    public void setSalary(String salary) {
        this.salary = salary;
    }

    public String getBenefit() {
        return benefit;
    }

    public void setBenefit(String benefit) {
        this.benefit = benefit;
    }

    public String getDescribe() {
        return describe;
    }

    public void setDescribe(String describe) {
        this.describe = describe;
    }

    public Double getLatitude() {
        return latitude;
    }

    public void setLatitude(Double latitude) {
        this.latitude = latitude;
    }

    public Double getLongitude() {
        return longitude;
    }

    public void setLongitude(Double longitude) {
        this.longitude = longitude;
    }

    public String getRequirements() {
        return requirements;
    }

    public void setRequirements(String requirements) {
        this.requirements = requirements;
    }

    public Long getStartDate() {
        return startDate;
    }

    public void setStartDate(Long startDate) {
        this.startDate = startDate;
    }

    public Long getEndDate() {
        return endDate;
    }

    public void setEndDate(Long endDate) {
        this.endDate = endDate;
    }

    @Override
    public int compareTo(Object o) {
        return ((JobDto) o).getStartDate() > getStartDate() ? 1 : -1;
    }
}
