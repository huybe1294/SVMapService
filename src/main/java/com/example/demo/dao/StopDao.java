package com.example.demo.dao;

import com.example.demo.dto.RouteDetailDto;
import com.example.demo.dto.StopDto;

import java.util.List;

public interface StopDao {
    List<StopDto> getStops();

    List<RouteDetailDto> getRouteDetailDto();
}
