package com.example.demo.dao;

import com.example.demo.dto.FavoriteJobDto;

import java.util.List;

public interface FavoriteJobDao {
    void createFavoriteJob(FavoriteJobDto favoriteJob);

    List<FavoriteJobDto> getFavoriteJob(Integer user_id);
}
