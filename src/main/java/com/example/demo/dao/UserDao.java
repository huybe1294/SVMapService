package com.example.demo.dao;

import com.example.demo.dto.UserDto;

import java.util.List;

public interface UserDao {
    List<UserDto> getUsers();

    UserDto getUserByEmail(String email);

    UserDto getUserById(int userId);

    void updateTokenUser(UserDto userDto);

    void createAccount(UserDto userDto);
}
