package com.example.demo.dao.impl;

import com.example.demo.dao.RouteDao;
import com.example.demo.dto.RouteDto;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.List;

@Repository
public class RouteDaoImpl implements RouteDao {
    @PersistenceContext
    private EntityManager entityManager;

    @Override
    @SuppressWarnings("unchecked")
    public List<RouteDto> getRoutes() {
        String hql = "from RouteDto";
        return (List<RouteDto>) entityManager.createQuery(hql).getResultList();
    }
}
