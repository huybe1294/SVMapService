package com.example.demo.dao.impl;

import com.example.demo.dao.BoardingHouseDao;
import com.example.demo.dto.BoardingHouseDto;
import org.hibernate.query.Query;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.List;

@Repository
@Transactional
public class BoardingHouseDaoImpl implements BoardingHouseDao {
    @PersistenceContext
    private EntityManager entityManager;

    @Override
    @SuppressWarnings("unchecked")
    public List<BoardingHouseDto> getBoardingHouseDtos() {
        String hql = "from BoardingHouseDto";
        return (List<BoardingHouseDto>) entityManager.createQuery(hql).getResultList();
    }

    @Override
    public BoardingHouseDto getHouseById(Integer id) {
        String hql = "from BoardingHouseDto where id=:id";
        Query query = (Query) entityManager.createQuery(hql);
        query.setParameter("id", id);
        List<BoardingHouseDto> boardingHouseDtos = query.list();
        if (boardingHouseDtos != null) {
            if (boardingHouseDtos.size() > 0) {
                BoardingHouseDto jobDto = boardingHouseDtos.get(0);
                return jobDto;
            }
        }
        return null;
    }

    @Override
    public List<BoardingHouseDto> getHouseFavorite() {
        String hql = "from BoardingHouseDto where favorite=1";
        return (List<BoardingHouseDto>) entityManager.createQuery(hql).getResultList();
    }

    @Override
    public void createBoardingHouse(BoardingHouseDto boardingHouseDto) {
        entityManager.persist(boardingHouseDto);
    }

    @Override
    public void updateBoardingHouseDto(BoardingHouseDto boardingHouseDto) {
        entityManager.merge(boardingHouseDto);
    }
}
