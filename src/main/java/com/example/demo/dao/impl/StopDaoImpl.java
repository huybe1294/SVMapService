package com.example.demo.dao.impl;

import com.example.demo.dao.StopDao;
import com.example.demo.dto.RouteDetailDto;
import com.example.demo.dto.StopDto;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.List;

@Repository
public class StopDaoImpl implements StopDao {
    @PersistenceContext
    private EntityManager entityManager;

    @Override
    @SuppressWarnings("unchecked")
    public List<StopDto> getStops() {
        String hql = "from StopDto";
        return (List<StopDto>) entityManager.createQuery(hql).getResultList();
    }

    @Override
    @SuppressWarnings("unchecked")
    public List<RouteDetailDto> getRouteDetailDto() {
        String hql = "from RouteDetailDto";
        return (List<RouteDetailDto>) entityManager.createQuery(hql).getResultList();
    }
}
