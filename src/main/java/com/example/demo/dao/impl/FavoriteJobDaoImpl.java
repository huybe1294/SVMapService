package com.example.demo.dao.impl;

import com.example.demo.dao.FavoriteJobDao;
import com.example.demo.dto.FavoriteJobDto;
import org.hibernate.query.Query;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.List;

@Transactional
@Repository
public class FavoriteJobDaoImpl implements FavoriteJobDao {
    @PersistenceContext
    private EntityManager entityManager;

    @Override
    public void createFavoriteJob(FavoriteJobDto favoriteJob) {
        entityManager.persist(favoriteJob);
    }

    @Override
    @SuppressWarnings("unchecked")
    public List<FavoriteJobDto> getFavoriteJob(Integer user_id) {
        String hql = "from FavoriteJobDto where user_id=:user_id";
        Query query = (Query) entityManager.createQuery(hql);
        query.setParameter("user_id", user_id);
        List<FavoriteJobDto> faviriteJobs = query.list();
        return faviriteJobs;
    }
}
