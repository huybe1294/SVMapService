package com.example.demo.dao.impl;

import com.example.demo.dao.FavoriteHouseDao;
import com.example.demo.dto.FavoriteHouseDto;
import org.hibernate.query.Query;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.List;

@Transactional
@Repository
public class FavoriteHouseDaoImpl implements FavoriteHouseDao {
    @PersistenceContext
    private EntityManager entityManager;

    @Override
    public void createFavoriteHouse(FavoriteHouseDto favoriteHouseDto) {
        entityManager.persist(favoriteHouseDto);
    }

    @Override
    @SuppressWarnings("unchecked")
    public List<FavoriteHouseDto> getFavoriteHouse(Integer user_id) {
        String hql = "from FavoriteHouseDto where user_id=:user_id";
        Query query = (Query) entityManager.createQuery(hql);
        query.setParameter("user_id", user_id);
        List<FavoriteHouseDto> favoriteHouseDtos = query.list();
        return favoriteHouseDtos;
    }
}
