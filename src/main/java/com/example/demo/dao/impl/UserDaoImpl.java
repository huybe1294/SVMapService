package com.example.demo.dao.impl;

import com.example.demo.dao.UserDao;
import com.example.demo.dto.UserDto;
import org.hibernate.query.Query;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.List;

@Transactional
@Repository
public class UserDaoImpl implements UserDao {

    @PersistenceContext
    private EntityManager entityManager;

    @SuppressWarnings("unchecked")
    @Override
    public List<UserDto> getUsers() {
        String hql = "from UserDto";
        return (List<UserDto>) entityManager.createQuery(hql).getResultList();
    }

    @Override
    @SuppressWarnings("unchecked")
    public UserDto getUserByEmail(String email) {
        String hql = "from UserDto where email=:email";
        Query query = (Query) entityManager.createQuery(hql);
        query.setParameter("email", email);
        List<UserDto> userDtos = query.list();
        if (userDtos != null) {
            if (userDtos.size() > 0) {
                UserDto userDto = userDtos.get(0);
                return userDto;
            }
        }
        return null;
    }

    @Override
    public UserDto getUserById(int id) {
        String hql = "select u from UserDto u where u.id=:id";
        Query query = (Query) entityManager.createQuery(hql);
        query.setParameter("id", id);
        List<UserDto> userDtos = query.list();
        if (userDtos != null) {
            if (userDtos.size() > 0) {
                UserDto userDto = userDtos.get(0);
                return userDto;
            }
        }
        return null;
    }

    @Override
    @SuppressWarnings("unchecked")
    public void updateTokenUser(UserDto userDto) {
        entityManager.merge(userDto);
    }

    @Override
    public void createAccount(UserDto userDto) {
        entityManager.persist(userDto);
    }
}
