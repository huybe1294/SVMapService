package com.example.demo.dao.impl;

import com.example.demo.dao.JobDao;
import com.example.demo.dto.FavoriteJobDto;
import com.example.demo.dto.JobDto;
import org.hibernate.query.Query;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.List;

@Repository
@Transactional
public class JobDaoImpl implements JobDao {
    @PersistenceContext
    private EntityManager entityManager;

    @Override
    @SuppressWarnings("unchecked")
    public List<JobDto> getJobDtos() {
        String hql = "from JobDto";
        return (List<JobDto>) entityManager.createQuery(hql).getResultList();
    }

    @Override
    public JobDto getJobById(Integer id) {
        String hql = "from JobDto where id=:id";
        Query query = (Query) entityManager.createQuery(hql);
        query.setParameter("id", id);
        List<JobDto> jobDtos = query.list();
        if (jobDtos != null) {
            if (jobDtos.size() > 0) {
                return jobDtos.get(0);
            }
        }
        return null;
    }

    @Override
    public List<JobDto> getJobFravorite() {
        String hql = "from JobDto where favorite=1";
        return (List<JobDto>) entityManager.createQuery(hql).getResultList();
    }

    @Override
    public void createJob(JobDto jobDto) {
        entityManager.persist(jobDto);
    }

    @Override
    public void updateJob(JobDto jobDto) {
        entityManager.merge(jobDto);
    }

}
