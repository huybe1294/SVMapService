package com.example.demo.dao;

import com.example.demo.dto.FavoriteHouseDto;

import java.util.List;

public interface FavoriteHouseDao {
    void createFavoriteHouse(FavoriteHouseDto favoriteHouseDto);

    List<FavoriteHouseDto> getFavoriteHouse(Integer user_id);
}
