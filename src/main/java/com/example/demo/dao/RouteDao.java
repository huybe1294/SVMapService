package com.example.demo.dao;

import com.example.demo.dto.RouteDto;

import java.util.List;

public interface RouteDao {
    List<RouteDto> getRoutes();
}
