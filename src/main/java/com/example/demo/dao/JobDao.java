package com.example.demo.dao;

import com.example.demo.dto.JobDto;

import java.util.List;

public interface JobDao {
    List<JobDto> getJobDtos();

    JobDto getJobById(Integer id);

    List<JobDto> getJobFravorite();

    void createJob(JobDto jobDto);

    void updateJob(JobDto jobDto);

}
