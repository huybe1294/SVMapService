package com.example.demo.dao;

import com.example.demo.dto.BoardingHouseDto;

import java.util.List;

public interface BoardingHouseDao {
    List<BoardingHouseDto> getBoardingHouseDtos();

    BoardingHouseDto getHouseById(Integer boardingHouseId);

    List<BoardingHouseDto> getHouseFavorite();

    void createBoardingHouse(BoardingHouseDto boardingHouseDto);

    void updateBoardingHouseDto(BoardingHouseDto boardingHouseDto);

}
