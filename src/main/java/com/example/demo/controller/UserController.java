package com.example.demo.controller;

import com.example.demo.dto.UserDto;
import com.example.demo.model.*;
import com.example.demo.service.UserService;
import com.example.demo.service.impl.SendingMailService;
import com.example.demo.util.CommonUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.Timer;
import java.util.TimerTask;

@Controller
@RequestMapping("/account")
public class UserController {
    @Autowired
    public UserService userService;

    @Autowired
    public SendingMailService sendingMailService;

    @RequestMapping(value = "/getUserById", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public UserDto getUsers(@RequestParam int userId) {
        return userService.getUserById(userId);
    }

    @RequestMapping(value = "/getUserByEmail", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public UserDto getUserByEmail(@RequestParam String email) {
        return userService.getUserByEmail(email);
    }

    @RequestMapping(value = "/login", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public ResponseEntity<LoginResponse> login(@RequestBody LoginParam param) {
        String email = param.getEmail();
        String password = param.getPassword();

        if (email == null || email.isEmpty() || password == null || password.isEmpty()) {
            return new ResponseEntity<>(HttpStatus.UNAUTHORIZED); //
        }
        String token = CommonUtils.generateRandomToken(32);
            UserDto userDto = userService.getUserByEmail(email);
        if (!password.equals(userDto.getPassword())) {
            return new ResponseEntity<>(HttpStatus.UNAUTHORIZED);//
        }
        userDto.setToken(token);
        userService.updateTokenUser(userDto);
        LoginResponse loginResponse = new LoginResponse(userDto.getId(), token, userDto.getEmail(), userDto.getFullName());
        return new ResponseEntity<LoginResponse>(loginResponse, HttpStatus.OK);//
    }

    @RequestMapping(value = "/register", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public ResponseEntity<BaseResponse> register(@RequestBody RegisterParam param) {
        String userName = param.getUserName();
        String email = param.getEmail();
        String password = param.getPassword();
        String phone = param.getPhone();
        String address = param.getAddress();

        if (userName == null || userName.isEmpty() || email == null || email.isEmpty() || password == null || password.isEmpty()) {
            return new ResponseEntity<>(HttpStatus.UNAUTHORIZED);
        }

        UserDto userDto = userService.getUserByEmail(email);
        if (userDto != null) {
            return new ResponseEntity<BaseResponse>(new BaseResponse("This email have been already registered"), HttpStatus.UNAUTHORIZED);
        }

        userDto = new UserDto();
        userDto.setFullName(userName);
        userDto.setEmail(email);
        userDto.setPassword(password);
        userDto.setPhone(phone);
        userDto.setAddress(address);

        userService.createAccount(userDto);

//        try {
//            sendingMailService.sendPlainTextEmail("huybe.1295@gmail.com", "Subject", "Successful Student Map account registration");
//        } catch (MessagingException e) {
//            e.printStackTrace();
//        }
        return new ResponseEntity<BaseResponse>(new BaseResponse("successfully !"), HttpStatus.OK);
    }

    @RequestMapping(value = "/forgot", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public ResponseEntity<ForgotResponse> forgotPassword(@RequestBody ForgotParam param) {
        if (param.getEmail() == null || param.getEmail().isEmpty()) {
            return new ResponseEntity<>(HttpStatus.UNAUTHORIZED);
        }
        String otpCode = CommonUtils.generateRandomToken(6);
        UserDto userDto = userService.getUserByEmail(param.getEmail());
        if (userDto == null) {
            return new ResponseEntity<>(HttpStatus.UNAUTHORIZED);
        }
        userDto.setOtpCode(otpCode);
        userService.updateTokenUser(userDto);

        new Timer().schedule(new TimerTask() {
            @Override
            public void run() {
                userDto.setOtpCode("");
                userService.updateTokenUser(userDto);
            }
        },30000);

//        try {
//            sendingMailService.sendPlainTextEmail("huybe.1295@gmail.com", "Subject", "OTPCode: " + otpCode);
//        } catch (MessagingException e) {
//            e.printStackTrace();
//        }

        return new ResponseEntity<ForgotResponse>(new ForgotResponse(userDto.getId()), HttpStatus.OK);
    }

    @RequestMapping(value = "/changePassOTP", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public ResponseEntity<BaseResponse> changePassOTP(@RequestBody ChangePassOTPParam param) {
        Integer userId = param.getUserId();
        String otpCode = param.getCode();
        String password = param.getPassword();

        if (userId == null  || password == null || password.isEmpty() || otpCode == null || otpCode.isEmpty()) {
            return new ResponseEntity<>(HttpStatus.UNAUTHORIZED);
        }
        if (userId < 0) {
            return new ResponseEntity<>(HttpStatus.UNAUTHORIZED);
        }

        UserDto userDto = userService.getUserById(userId);
        if (userDto == null) {
            return new ResponseEntity<>(HttpStatus.UNAUTHORIZED);
        }
        if (!userDto.getOtpCode().equals(otpCode)) {
            return new ResponseEntity<>(HttpStatus.UNAUTHORIZED);
        }
        userDto.setPassword(password);
        userService.updateTokenUser(userDto);
//        try {
//            sendingMailService.sendPlainTextEmail("huybe.1295@gmail.com", "Subject", "OTPCode: " + otpCode);
//        } catch (MessagingException e) {
//            e.printStackTrace();
//        }
        return new ResponseEntity<BaseResponse>(new BaseResponse("Change pass successful"), HttpStatus.OK);
    }
}
