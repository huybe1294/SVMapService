package com.example.demo.controller;

import com.example.demo.dto.RouteDto;
import com.example.demo.service.RouteService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.List;

@Controller
public class RouteController {
    @Autowired
    public RouteService routeService;

    @RequestMapping(value = "/map/getRoute", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public List<RouteDto> getRoute() {
        return routeService.getRoutes();
    }
}
