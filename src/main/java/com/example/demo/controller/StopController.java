package com.example.demo.controller;

import com.example.demo.dto.RouteDetailDto;
import com.example.demo.dto.RouteDto;
import com.example.demo.dto.StopDto;
import com.example.demo.model.Stop;
import com.example.demo.service.RouteService;
import com.example.demo.service.StopService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.ArrayList;
import java.util.List;

@Controller
public class StopController {
    @Autowired
    public StopService stopService;
    @Autowired
    public RouteService routeService;

    @RequestMapping(value = "/map/getStop", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public List<StopDto> getStop() {
        return stopService.getStops();
    }

    @RequestMapping(value = "/map/getRouteDetail", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public List<Stop> getRouteDetail() {
        List<StopDto> stopDto = stopService.getStops();
        List<RouteDto> routeDtos = routeService.getRoutes();
        List<RouteDetailDto> routeDetailDtos = stopService.getRouteDetailDto();

        List<Stop> stops = new ArrayList<>();
        List<String> listRoute;

        for (int i = 0; i < stopDto.size(); i++) {
            listRoute = new ArrayList<>();
            Stop stop = new Stop();
            stop.setStopName(stopDto.get(i).getStopName()); //1
            stop.setLatitude(stopDto.get(i).getLatitude()); //2
            stop.setLongitude(stopDto.get(i).getLongitude()); //3

            for (int j = 0; j < routeDetailDtos.size(); j++) {
                if (stopDto.get(i).getStopId() == routeDetailDtos.get(j).getStopId()) {
                    for (int k = 0; k < routeDtos.size(); k++) {
                        if (routeDetailDtos.get(j).getRouteId() == routeDtos.get(k).getRouteId()) {
                            int index = k;
                            String routeName = routeDtos.get(index).getRouteName();
                            listRoute.add(routeName);
                        }
                    }
                    stop.setRoutes(listRoute);//4
                }
            }
            stops.add(stop);
        }
        return stops;
    }
}
