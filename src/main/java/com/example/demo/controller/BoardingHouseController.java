package com.example.demo.controller;

import com.example.demo.dto.BoardingHouseDto;
import com.example.demo.dto.FavoriteHouseDto;
import com.example.demo.model.BaseResponse;
import com.example.demo.model.FollowHouseParam;
import com.example.demo.model.HouseParam;
import com.example.demo.service.BoardingHouseService;
import com.example.demo.service.FavoriteHouseService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

@Controller
public class BoardingHouseController {
    @Autowired
    private BoardingHouseService boardingHouseService;

    @Autowired
    private FavoriteHouseService favoriteHouseService;

    @RequestMapping(value = "/map/getBoardingHouse", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public List<BoardingHouseDto> getBoardingHouse() {
        return boardingHouseService.getBoardingHouseDtos();
    }

    @RequestMapping(value = "/map/getHouseByDate", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public List<BoardingHouseDto> getHouseByDate() {
        List<BoardingHouseDto> houseDtos = boardingHouseService.getBoardingHouseDtos();
        Collections.sort(houseDtos);
        return houseDtos;
    }

    @RequestMapping(value = "/map/getBoardingHouseByDate", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public List<BoardingHouseDto> getBoardingHouseByDate() {
        List<BoardingHouseDto> boardingHouseDtos = boardingHouseService.getBoardingHouseDtos();
        Collections.sort(boardingHouseDtos);
        return boardingHouseDtos;
    }

    @RequestMapping(value = "/map/updateHouse", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public ResponseEntity<BaseResponse> updateHouse(@RequestBody HouseParam param) {
        BoardingHouseDto boardingHouseDto = new BoardingHouseDto();

        boardingHouseDto.setTitle(param.getTitle());
        boardingHouseDto.setPhone(param.getPhone());
        boardingHouseDto.setAddress(param.getAddress());
        boardingHouseDto.setArea(param.getArea());
        boardingHouseDto.setPrice(param.getPrice());
        boardingHouseDto.setDescribe(param.getDescribe());
        boardingHouseDto.setLatitude(param.getLatitude());
        boardingHouseDto.setLongitude(param.getLongitude());
        boardingHouseDto.setStartDate(param.getStartDate());
        boardingHouseDto.setEndDate(param.getEndDate());

        boardingHouseService.createHouse(boardingHouseDto);

        return new ResponseEntity<BaseResponse>(new BaseResponse("successfully !"), HttpStatus.OK);
    }


    @RequestMapping(value = "/map/getFavoriteHouse", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public List<BoardingHouseDto> getFavoriteHouse(@RequestParam Integer user_id) {
        List<FavoriteHouseDto> favoriteHouseDtos = favoriteHouseService.getFavoriteHouse(user_id);
        List<BoardingHouseDto> houseDtos = new ArrayList<>();

        if (favoriteHouseDtos != null) {
            for (int i = 0; i < favoriteHouseDtos.size(); i++) {
                int job_id = favoriteHouseDtos.get(i).getHouse_id();
                BoardingHouseDto  houseDto= boardingHouseService.getHouseById(job_id);
                if (houseDto != null) {
                    houseDtos.add(houseDto);
                }
            }
        }
        return houseDtos;
    }

    @RequestMapping(value = "/map/followHouse", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public ResponseEntity<BaseResponse> followHouse(@RequestBody FollowHouseParam param) {
        Integer user_id = param.getUser_id();
        Integer house_id = param.getHouse_id();

        if (user_id == null || house_id == null) {
            return new ResponseEntity<>(HttpStatus.UNAUTHORIZED);
        }
        FavoriteHouseDto favoriteHouseDto = new FavoriteHouseDto(user_id, house_id);
        favoriteHouseService.createFavoriteHouse(favoriteHouseDto);

        return new ResponseEntity<BaseResponse>(new BaseResponse("successful"), HttpStatus.OK);
    }

}
