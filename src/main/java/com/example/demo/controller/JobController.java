package com.example.demo.controller;

import com.example.demo.dto.FavoriteJobDto;
import com.example.demo.dto.JobDto;
import com.example.demo.model.BaseResponse;
import com.example.demo.model.FollowJobParam;
import com.example.demo.model.JobParam;
import com.example.demo.service.FavoriteJobService;
import com.example.demo.service.JobService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

@Controller
public class JobController {
    @Autowired
    public JobService jobService;

    @Autowired
    private FavoriteJobService favoriteJobService;

    @RequestMapping(value = "/map/getJob", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public List<JobDto> getJobs() {
        return jobService.getJobDtos();
    }

    @RequestMapping(value = "/map/updateJob", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public ResponseEntity<BaseResponse> updateJob(@RequestBody JobParam param) {
        JobDto jobDto = new JobDto();

        jobDto.setTitle(param.getTitle());
        jobDto.setPhone(param.getPhone());
        jobDto.setPhone(param.getPhone());
        jobDto.setAddress(param.getAddress());
        jobDto.setCompanyName(param.getCompanyName());
        jobDto.setDescribe(param.getDescribe());
        jobDto.setBenefit(param.getBenefit());
        jobDto.setSalary(param.getSalary());
        jobDto.setLatitude(param.getLatitude());
        jobDto.setLongitude(param.getLongitude());
        jobDto.setRequirements(param.getRequirements());
        jobDto.setStartDate(param.getStartDate());
        jobDto.setEndDate(param.getEndDate());

        jobService.createJob(jobDto);

        return new ResponseEntity<BaseResponse>(new BaseResponse("successfully !"), HttpStatus.OK);
    }

    @RequestMapping(value = "/map/getJobByDate", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public List<JobDto> getJobsByDate() {
        List<JobDto> jobDtos = jobService.getJobDtos();
        Collections.sort(jobDtos);
        return jobDtos;
    }

    @RequestMapping(value = "/map/getFavoriteJob", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public List<JobDto> getFavoriteJob(@RequestParam Integer user_id) {
        List<FavoriteJobDto> favoriteJobDtos = favoriteJobService.getFavoriteJob(user_id);
        List<JobDto> jobDtos = new ArrayList<>();

        if (favoriteJobDtos != null) {
            for (int i = 0; i < favoriteJobDtos.size(); i++) {
                int job_id = favoriteJobDtos.get(i).getJob_id();
                JobDto jobDto = jobService.getJobById(job_id);
                if (jobDto != null) {
                    jobDtos.add(jobDto);
                }
            }
        }
        return jobDtos;
    }

    @RequestMapping(value = "/map/followJob", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public ResponseEntity<BaseResponse> followJob(@RequestBody FollowJobParam param) {
        Integer user_id = param.getUser_id();
        Integer job_id = param.getJob_id();

        if (user_id == null || job_id == null) {
            return new ResponseEntity<>(HttpStatus.UNAUTHORIZED);
        }
        FavoriteJobDto faviriteJob = new FavoriteJobDto(user_id, job_id);
        favoriteJobService.createFavoriteJob(faviriteJob);

        return new ResponseEntity<BaseResponse>(new BaseResponse("successful"), HttpStatus.OK);
    }
}
