package com.example.demo.service;

import com.example.demo.dto.UserDto;

import java.util.List;

public interface UserService {
    List<UserDto> getUsers();

    UserDto getUserByEmail(String email);

    UserDto getUserById(int id);

    void updateTokenUser(UserDto userDto);

    void createAccount(UserDto userDto);
}
