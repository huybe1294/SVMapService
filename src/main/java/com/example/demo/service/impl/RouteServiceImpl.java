package com.example.demo.service.impl;

import com.example.demo.dao.RouteDao;
import com.example.demo.dto.RouteDto;
import com.example.demo.service.RouteService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class RouteServiceImpl implements RouteService {
    @Autowired
    private RouteDao routeDao;

    @Override
    public List<RouteDto> getRoutes() {
        return routeDao.getRoutes();
    }
}
