package com.example.demo.service.impl;

import com.example.demo.dao.FavoriteJobDao;
import com.example.demo.dto.FavoriteJobDto;
import com.example.demo.service.FavoriteJobService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class FavoriteJobServiceImpl implements FavoriteJobService {
    @Autowired
    private FavoriteJobDao favoriteJobDao;

    @Override
    public void createFavoriteJob(FavoriteJobDto favoriteJob) {
        favoriteJobDao.createFavoriteJob(favoriteJob);
    }

    @Override
    public List<FavoriteJobDto> getFavoriteJob(Integer user_id) {
        return favoriteJobDao.getFavoriteJob(user_id);
    }
}
