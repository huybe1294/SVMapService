package com.example.demo.service.impl;

import com.example.demo.dao.StopDao;
import com.example.demo.dto.RouteDetailDto;
import com.example.demo.dto.StopDto;
import com.example.demo.service.StopService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class StopServiceImpl implements StopService {
    @Autowired
    private StopDao stopDao;

    @Override
    public List<StopDto> getStops() {
        return stopDao.getStops();
    }

    @Override
    public List<RouteDetailDto> getRouteDetailDto() {
        return stopDao.getRouteDetailDto();
    }
}
