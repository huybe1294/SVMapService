package com.example.demo.service.impl;

import com.example.demo.dao.UserDao;
import com.example.demo.dto.UserDto;
import com.example.demo.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class UserServiceImpl implements UserService {
    @Autowired
    private UserDao userDao;

    @Override
    public List<UserDto> getUsers() {
        return userDao.getUsers();
    }

    @Override
    public UserDto getUserByEmail(String email) {
        return userDao.getUserByEmail(email);
    }

    @Override
    public UserDto getUserById(int id) {
        return userDao.getUserById(id);
    }

    @Override
    public void updateTokenUser(UserDto userDto) {
         userDao.updateTokenUser(userDto);
    }

    @Override
    public void createAccount(UserDto userDto) {
         userDao.createAccount(userDto);
    }

}
