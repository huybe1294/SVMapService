package com.example.demo.service.impl;

import com.example.demo.dao.JobDao;
import com.example.demo.dto.JobDto;
import com.example.demo.service.JobService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class JobServiceImpl implements JobService {
    @Autowired
    private JobDao jobDao;

    @Override
    public List<JobDto> getJobDtos() {
        return jobDao.getJobDtos();
    }

    @Override
    public JobDto getJobById(Integer id) {
        return jobDao.getJobById(id);
    }

    @Override
    public List<JobDto> getJobFavorite() {
        return jobDao.getJobFravorite();
    }

    @Override
    public void createJob(JobDto jobDto) {
        jobDao.createJob(jobDto);
    }

    @Override
    public void updateJob(JobDto jobDto) {
        jobDao.updateJob(jobDto);
    }

}
