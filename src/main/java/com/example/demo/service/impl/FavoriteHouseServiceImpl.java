package com.example.demo.service.impl;

import com.example.demo.dao.FavoriteHouseDao;
import com.example.demo.dto.FavoriteHouseDto;
import com.example.demo.service.FavoriteHouseService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class FavoriteHouseServiceImpl implements FavoriteHouseService {
    @Autowired
    private FavoriteHouseDao favoriteHouseDao;

    @Override
    public void createFavoriteHouse(FavoriteHouseDto favoriteHouseDto) {
        favoriteHouseDao.createFavoriteHouse(favoriteHouseDto);
    }

    @Override
    public List<FavoriteHouseDto> getFavoriteHouse(Integer user_id) {
        return favoriteHouseDao.getFavoriteHouse(user_id);
    }
}
