package com.example.demo.service.impl;

import com.example.demo.dao.BoardingHouseDao;
import com.example.demo.dto.BoardingHouseDto;
import com.example.demo.service.BoardingHouseService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class BoardingHouseServiceImpl implements BoardingHouseService {
    @Autowired
    private BoardingHouseDao boardingHouseDao;

    @Override
    public List<BoardingHouseDto> getBoardingHouseDtos() {
        return boardingHouseDao.getBoardingHouseDtos();
    }

    @Override
    public BoardingHouseDto getHouseById(Integer houseId) {
        return boardingHouseDao.getHouseById(houseId);
    }

    @Override
    public List<BoardingHouseDto> getHouseFavorite( ) {
        return boardingHouseDao.getHouseFavorite();
    }

    @Override
    public void createHouse(BoardingHouseDto boardingHouseDto) {
        boardingHouseDao.createBoardingHouse(boardingHouseDto);
    }

    @Override
    public void updateBoardingHouseDto(BoardingHouseDto boardingHouseDto) {
        boardingHouseDao.updateBoardingHouseDto(boardingHouseDto);
    }
}
