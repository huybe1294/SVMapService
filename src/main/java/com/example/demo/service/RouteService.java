package com.example.demo.service;

import com.example.demo.dto.RouteDto;

import java.util.List;

public interface RouteService {
    List<RouteDto> getRoutes();
}
