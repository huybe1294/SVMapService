package com.example.demo.service;

import com.example.demo.dto.BoardingHouseDto;

import java.util.List;

public interface BoardingHouseService {
    List<BoardingHouseDto> getBoardingHouseDtos();

    BoardingHouseDto getHouseById(Integer houseId);

    List<BoardingHouseDto> getHouseFavorite();

    void createHouse(BoardingHouseDto boardingHouseDto);

    void updateBoardingHouseDto(BoardingHouseDto boardingHouseDto);
}
