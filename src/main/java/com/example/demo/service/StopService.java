package com.example.demo.service;

import com.example.demo.dto.RouteDetailDto;
import com.example.demo.dto.StopDto;

import java.util.List;

public interface StopService {
    List<StopDto> getStops();
    List<RouteDetailDto> getRouteDetailDto();
}
