package com.example.demo.service;

import com.example.demo.dto.JobDto;

import java.util.List;

public interface JobService {
    List<JobDto> getJobDtos();

    JobDto getJobById(Integer id);

    List<JobDto> getJobFavorite();

    void createJob(JobDto jobDto);


    void updateJob(JobDto jobDto);

}
