package com.example.demo.service;

import com.example.demo.dto.FavoriteHouseDto;

import java.util.List;

public interface FavoriteHouseService {
    void createFavoriteHouse(FavoriteHouseDto favoriteHouseDto);

    List<FavoriteHouseDto> getFavoriteHouse(Integer user_id);
}
