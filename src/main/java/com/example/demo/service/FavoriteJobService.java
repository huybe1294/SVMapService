package com.example.demo.service;

import com.example.demo.dto.FavoriteJobDto;

import java.util.List;

public interface FavoriteJobService {
    void createFavoriteJob(FavoriteJobDto favoriteJob);

    List<FavoriteJobDto> getFavoriteJob(Integer user_id);
}
