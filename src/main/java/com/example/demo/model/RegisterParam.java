package com.example.demo.model;

public class RegisterParam {
    private String userName;
    private String email;
    private String password;
    private String phone;
    private String address;

    public RegisterParam() {
    }

    public String getUserName() {
        return userName;
    }

    public String getEmail() {
        return email;
    }

    public String getPassword() {
        return password;
    }

    public String getPhone() {
        return phone;
    }

    public String getAddress() {
        return address;
    }
}
