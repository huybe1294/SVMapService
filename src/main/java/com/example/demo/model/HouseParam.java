package com.example.demo.model;

public class HouseParam {
    private String title;
    private String address;
    private Integer area;
    private String phone;
    private String price;
    private String describe;
    private Double latitude;
    private Double longitude;
    private Long startDate;
    private Long endDate;

    public HouseParam() {
    }

    public HouseParam (String title, String address, Integer area, String phone, String price, String describe, Double latitude, Double longitude, Long startDate, Long endDate) {
        this.title = title;
        this.address = address;
        this.area = area;
        this.phone = phone;
        this.price = price;
        this.describe = describe;
        this.latitude = latitude;
        this.longitude = longitude;
        this.startDate = startDate;
        this.endDate = endDate;
    }

    public Integer getArea() {
        return area;
    }

    public String getTitle() {
        return title;
    }

    public String getAddress() {
        return address;
    }

    public String getPhone() {
        return phone;
    }

    public String getPrice() {
        return price;
    }

    public String getDescribe() {
        return describe;
    }

    public Double getLatitude() {
        return latitude;
    }

    public Double getLongitude() {
        return longitude;
    }

    public Long getStartDate() {
        return startDate;
    }

    public Long getEndDate() {
        return endDate;
    }
}
