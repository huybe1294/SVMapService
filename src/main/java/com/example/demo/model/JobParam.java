package com.example.demo.model;

public class JobParam {
    private String title;
    private String address;
    private String companyName;
    private String phone;
    private String salary;
    private String describe;
    private String benefit;
    private Double latitude;
    private Double longitude;
    private String requirements;
    private Long startDate;
    private Long endDate;

    public JobParam() {
    }

    public JobParam(String title, String address, String companyName, String phone, String salary, String describe, String befenit, Double latitude, Double longitude, String requirements, Long startDate, Long endDate) {
        this.title = title;
        this.address = address;
        this.companyName = companyName;
        this.phone = phone;
        this.salary = salary;
        this.describe = describe;
        this.benefit = befenit;
        this.latitude = latitude;
        this.longitude = longitude;
        this.requirements = requirements;
        this.startDate = startDate;
        this.endDate = endDate;
    }


    public String getTitle() {
        return title;
    }

    public String getAddress() {
        return address;
    }

    public String getCompanyName() {
        return companyName;
    }

    public String getPhone() {
        return phone;
    }

    public String getSalary() {
        return salary;
    }

    public String getDescribe() {
        return describe;
    }

    public String getBenefit() {
        return benefit;
    }

    public Double getLatitude() {
        return latitude;
    }

    public Double getLongitude() {
        return longitude;
    }

    public String getRequirements() {
        return requirements;
    }

    public Long getStartDate() {
        return startDate;
    }

    public Long getEndDate() {
        return endDate;
    }
}
