package com.example.demo.model;

public class ForgotResponse {
    private Integer id;

    public ForgotResponse() {
    }

    public ForgotResponse(Integer id) {
        this.id = id;
    }

    public Integer getId() {
        return id;
    }
}
