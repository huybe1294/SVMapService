package com.example.demo.model;

public class ForgotParam {
    private String email;

    public ForgotParam() {
    }

    public ForgotParam(String email) {
        this.email = email;
    }
    public String getEmail() {
        return email;
    }
}
