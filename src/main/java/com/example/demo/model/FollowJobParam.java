package com.example.demo.model;

public class FollowJobParam {
    private Integer user_id;
    private Integer job_id;

    public FollowJobParam() {
    }

    public FollowJobParam(Integer user_id, Integer job_id) {
        this.user_id = user_id;
        this.job_id = job_id;
    }

    public Integer getUser_id() {
        return user_id;
    }

    public Integer getJob_id() {
        return job_id;
    }
}
