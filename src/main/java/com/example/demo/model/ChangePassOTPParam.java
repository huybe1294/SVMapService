package com.example.demo.model;

public class ChangePassOTPParam {
    private Integer userId;
    private String code;
    private String password;

    public ChangePassOTPParam() {
    }

    public ChangePassOTPParam(Integer id, String code, String password) {
        this.userId = id;
        this.code = code;
        this.password = password;
    }

    public Integer getUserId() {
        return userId;
    }

    public String getCode() {
        return code;
    }

    public String getPassword() {
        return password;
    }
}
