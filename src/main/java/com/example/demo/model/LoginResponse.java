package com.example.demo.model;

public class LoginResponse {
    private int userId;
    private String token;
    private String email;
    private String fullName;

    public LoginResponse() {
    }

    public LoginResponse(int userId, String token, String email, String fullName) {
        this.userId = userId;
        this.token = token;
        this.email = email;
        this.fullName = fullName;
    }

    public int getUserId() {
        return userId;
    }

    public String getToken() {
        return token;
    }

    public String getEmail() {
        return email;
    }

    public String getFullName() {
        return fullName;
    }
}
