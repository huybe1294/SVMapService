package com.example.demo.model;

public class LoginParam {
    private String email;
    private String password;

    public LoginParam() {
    }

    public LoginParam(String email, String password) {
        this.email = email;
        this.password = password;
    }

    public String getEmail() {
        return email;
    }

    public String getPassword() {
        return password;
    }
}
