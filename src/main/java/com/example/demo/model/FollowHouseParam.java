package com.example.demo.model;

public class FollowHouseParam {
    private Integer user_id;
    private Integer house_id;

    public FollowHouseParam() {
    }

    public FollowHouseParam(Integer user_id, Integer job_id) {
        this.user_id = user_id;
        this.house_id = job_id;
    }

    public Integer getUser_id() {
        return user_id;
    }

    public Integer getHouse_id() {
        return house_id;
    }
}
