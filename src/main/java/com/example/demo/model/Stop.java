package com.example.demo.model;

import java.util.List;

public class Stop {
    private String stopName;
    private List<String> routes;
    private String latitude;
    private String longitude;

    public Stop() {
    }

    public Stop(String stopName, List<String> routes, String latitude, String longitude) {
        this.stopName = stopName;
        this.routes = routes;
        this.latitude = latitude;
        this.longitude = longitude;
    }

    public void setStopName(String stopName) {
        this.stopName = stopName;
    }

    public void setRoutes(List<String> routes) {
        this.routes = routes;
    }

    public void setLatitude(String latitude) {
        this.latitude = latitude;
    }

    public void setLongitude(String longitude) {
        this.longitude = longitude;
    }

    public String getStopName() {
        return stopName;
    }

    public List<String> getRoutes() {
        return routes;
    }

    public String getLatitude() {
        return latitude;
    }

    public String getLongitude() {
        return longitude;
    }
}
